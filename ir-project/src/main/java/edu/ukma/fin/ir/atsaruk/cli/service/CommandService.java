package edu.ukma.fin.ir.atsaruk.cli.service;

import edu.ukma.fin.ir.atsaruk.cli.entity.Command;
import java.util.List;
import java.util.function.Function;

public interface CommandService extends Function<Command, Boolean> {

  String commandName();

  String commandDescription();

  List<String> subCommandList();

  String commandInfo();
}
