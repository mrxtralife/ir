package edu.ukma.fin.ir.atsaruk.cli.service.loaddir;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.POSITIONAL_SUB_COMMAND;
import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import edu.ukma.fin.ir.atsaruk.cli.service.SubCommandService;
import edu.ukma.fin.ir.atsaruk.dictionary.service.PositionalWordDictionaryCatalogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class LoadDirectoryToPositionalWordDictionaryCatalogSubCommandService implements
    SubCommandService {

  private static final Logger LOGGER = LoggerFactory.getLogger(
      LoadDirectoryToPositionalWordDictionaryCatalogSubCommandService.class);
  private static final String subCommandDescription = "Loads directory content and creates positional dictionary catalog";

  private final PositionalWordDictionaryCatalogService catalogService;

  public LoadDirectoryToPositionalWordDictionaryCatalogSubCommandService(
      final PositionalWordDictionaryCatalogService catalogService) {
    this.catalogService = catalogService;
  }

  @Override
  public Boolean apply(SubCommand subCommand) {
    LOGGER.debug("In load-dir positional sub command service");

    for (final String directory : subCommand.arguments().split("\\s+")) {
      catalogService.loadDirectoryContent(directory);
    }

    return true;
  }

  @Override
  public String subCommandName() {
    return POSITIONAL_SUB_COMMAND;
  }

  @Override
  public String subCommandDescription() {
    return subCommandDescription;
  }

  @Override
  public String subCommandInfo() {
    return format("\t%s\t\t%s%n", subCommandName(), subCommandDescription());
  }
}
