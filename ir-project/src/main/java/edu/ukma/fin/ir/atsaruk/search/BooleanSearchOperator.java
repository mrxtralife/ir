package edu.ukma.fin.ir.atsaruk.search;

public enum BooleanSearchOperator {
  AND, OR
}
