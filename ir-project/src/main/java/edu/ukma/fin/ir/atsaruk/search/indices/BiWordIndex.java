package edu.ukma.fin.ir.atsaruk.search.indices;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableCollection;
import static java.util.Collections.unmodifiableList;

import edu.ukma.fin.ir.atsaruk.dictionary.BiWord;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.springframework.stereotype.Component;

@Component
public class BiWordIndex implements Index<BiWord, String> {

  private final Map<BiWord, List<String>> index = new TreeMap<>();
  private final Set<String> values = new TreeSet<>();

  @Override
  public void add(BiWord key, String... values) {
    add(key, asList(values));
  }

  @Override
  public void add(BiWord key, Collection<String> values) {
    final List<String> currentValues = getOrEmpty(key);
    currentValues.addAll(values);
    index.put(key, currentValues);
    this.values.addAll(values);
  }

  @Override
  public void add(Map<BiWord, Collection<String>> entries) {
    for (Entry<BiWord, Collection<String>> entry : entries.entrySet()) {
      add(entry.getKey(), entry.getValue());
    }
  }

  @Override
  public void remove(BiWord key, String... values) {
    remove(key, asList(values));
  }

  @Override
  public void remove(BiWord key, Collection<String> values) {
    final List<String> currentValues = getOrEmpty(key);
    currentValues.removeAll(values);
  }

  @Override
  public void remove(Map<BiWord, Collection<String>> entries) {
    for (Entry<BiWord, Collection<String>> entry : entries.entrySet()) {
      remove(entry.getKey(), entry.getValue());
    }
  }

  @Override
  public Collection<BiWord> getKeys() {
    return unmodifiableCollection(index.keySet());
  }

  @Override
  public List<String> search(final BiWord keyword) {
    final List<String> searchResult = index.get(keyword);
    return unmodifiableList(searchResult != null ? searchResult : emptyList());
  }

  @Override
  public List<String> searchNot(final BiWord keyword) {
    final List<String> positiveSearchResult = search(keyword);
    final List<String> searchResult = new ArrayList<>(values);
    searchResult.removeAll(positiveSearchResult);
    return unmodifiableList(searchResult);
  }

  private List<String> getOrEmpty(final BiWord key) {
    final List<String> values = index.get(key);
    return values != null ? values : new ArrayList<>();
  }
}
