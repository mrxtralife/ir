package edu.ukma.fin.ir.atsaruk.dictionary;

import java.util.List;

public final class PostProcessorUtils {

  private PostProcessorUtils() {
    //Util class
  }

  public static <T> PostProcessor<T> asPipe(final List<PostProcessor<T>> postProcessors) {
    return postProcessors.stream().reduce(PostProcessor::andThen).orElse((T t) -> t);
  }
}
