package edu.ukma.fin.ir.atsaruk.cli;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.printError;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.printPrompt;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.printStartLine;

import edu.ukma.fin.ir.atsaruk.cli.entity.Command;
import edu.ukma.fin.ir.atsaruk.common.Dispatcher;
import edu.ukma.fin.ir.atsaruk.common.Parser;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CommandLineInteractiveClient {

  private static Logger LOGGER = LoggerFactory.getLogger(CommandLineInteractiveClient.class);

  private final boolean exitOnError;
  private final Parser<String, Command> parser;
  private final Dispatcher<Command> dispatcher;

  public CommandLineInteractiveClient(@Value("${ir.exitOnError}") final boolean exitOnError,
      final Parser<String, Command> parser,
      final Dispatcher<Command> dispatcher) {
    this.exitOnError = exitOnError;
    this.dispatcher = dispatcher;
    this.parser = parser;

  }

  public void interact() {
    LOGGER.debug("Starting interactive mode");

    printStartLine();

    final Scanner scanner = new Scanner(System.in);
    boolean interact = true;

    while (interact) {
      printPrompt();
      final String commandStr = scanner.nextLine();
      try {
        final Command command = parser.parse(commandStr);
        interact = dispatcher.dispatch(command);
      } catch (CliException ex) {
        printError(ex.getMessage());
        if (exitOnError) {
          interact = false;
        }
      } catch (Throwable ex) {
        ex.printStackTrace();
        printError("Unknown error: " + ex.getMessage());
        if (exitOnError) {
          interact = false;
        }
      }
    }

    scanner.close();
    LOGGER.debug("Exiting interactive mode");
  }
}
