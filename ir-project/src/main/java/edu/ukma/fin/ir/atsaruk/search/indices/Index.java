package edu.ukma.fin.ir.atsaruk.search.indices;

import java.util.Collection;
import java.util.Map;

public interface Index<K, V> extends Searchable<K, V> {

  void add(final K key, final V... values);

  void add(final K key, final Collection<V> values);

  void add(final Map<K, Collection<V>> entries);

  void remove(final K key, final V... values);

  void remove(final K key, final Collection<V> values);

  void remove(final Map<K, Collection<V>> entries);

  Collection<K> getKeys();
}
