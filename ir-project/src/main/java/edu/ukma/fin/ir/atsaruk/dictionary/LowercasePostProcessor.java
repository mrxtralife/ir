package edu.ukma.fin.ir.atsaruk.dictionary;

import org.springframework.stereotype.Component;

@Component
public class LowercasePostProcessor implements PostProcessor<String> {

  @Override
  public String apply(String s) {
    if (s == null) {
      return "";
    }
    return s.toLowerCase();
  }
}
