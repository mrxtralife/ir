package edu.ukma.fin.ir.atsaruk.cli;

import static java.lang.String.format;

public final class CliUtils {

  public static final String EMPTY_STRING = "";
  public static final String HELP_COMMAND = "help";
  public static final String EXIT_COMMAND = "exit";

  public static final String LOAD_DIRECTORY_COMMAND = "load-dir";
  public static final String STRING_SUB_COMMAND = "string";
  public static final String BIWORD_SUB_COMMAND = "biword";
  public static final String POSITIONAL_SUB_COMMAND = "positional";

  public static final String CREATE_INDEX_COMMAND = "create-index";
  public static final String INVERTED_INDEX_SUB_COMMAND = "inverted-index";
  public static final String INCIDENCE_MATRIX_SUB_COMMAND = "incidence-matrix";

  public static final String SIMPLE_SEARCH_COMMAND = "search-simple";

  public static final String SEARCH_COMMAND = "search";

  private static final String ANSI_RESET = "\u001B[0m";
  private static final String ANSI_CYAN = "\u001B[36m";
  private static final String ANSI_RED = "\u001B[31m";

  private static final String START_LINE = format("%n"
      + "This is interactive information retrieval application%n"
      + "Enter 'help' to get list of commands or 'exit' to quit");
  private static final String DEFAULT_PROMPT = "ir >> ";

  private CliUtils() {
    //Util class
  }

  static void printStartLine() {
    System.out.println(START_LINE);
  }

  static void printPrompt(final String prompt) {
    System.out.print(ANSI_CYAN + prompt + ANSI_RESET);
  }

  static void printPrompt() {
    printPrompt(DEFAULT_PROMPT);
  }

  static void printError(final String error) {
    System.out.println(ANSI_RED + error + ANSI_RESET);
  }

  static String valueOrEmpty(final String str) {
    return str == null ? EMPTY_STRING : str;
  }

  public static <T> void printToConsole(final Iterable<T> iterable) {
    for (final T item : iterable) {
      System.out.println(item);
    }
  }
}
