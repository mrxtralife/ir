package edu.ukma.fin.ir.atsaruk.dictionary;

import org.immutables.value.Value.Immutable;

@Immutable
public interface BiWord extends Comparable<BiWord> {

  String first();

  String second();

  @Override
  default int compareTo(final BiWord other) {
    int firstCompare = this.first().compareTo(other.first());
    if (firstCompare != 0) {
      return firstCompare;
    }
    return this.second().compareTo(other.second());
  }
}
