package edu.ukma.fin.ir.atsaruk.cli.entity;

import org.immutables.value.Value.Immutable;

@Immutable
public interface SubCommand {

  String subCommand();

  String arguments();

}
