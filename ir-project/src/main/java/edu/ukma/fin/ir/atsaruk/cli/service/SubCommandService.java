package edu.ukma.fin.ir.atsaruk.cli.service;

import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import java.util.function.Function;

public interface SubCommandService extends Function<SubCommand, Boolean> {

  String subCommandName();

  String subCommandDescription();

  String subCommandInfo();
}
