package edu.ukma.fin.ir.atsaruk.dictionary;

import static java.util.Collections.unmodifiableCollection;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

class StringDictionary implements Dictionary<String> {

  private final Set<String> dictionary = new TreeSet<>();

  @Override
  public void add(String token) {
    dictionary.add(token);
  }

  @Override
  public void add(Collection<String> tokens) {
    dictionary.addAll(tokens);
  }

  @Override
  public void remove(String token) {
    dictionary.remove(token);
  }

  @Override
  public void remove(Collection<String> tokens) {
    dictionary.removeAll(tokens);
  }

  @Override
  public Collection<String> getTokens() {
    return unmodifiableCollection(dictionary);
  }
}
