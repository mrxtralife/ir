package edu.ukma.fin.ir.atsaruk.cli.service.search;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.BIWORD_SUB_COMMAND;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.printToConsole;
import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.CliException;
import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import edu.ukma.fin.ir.atsaruk.cli.service.SubCommandService;
import edu.ukma.fin.ir.atsaruk.search.Searcher;
import edu.ukma.fin.ir.atsaruk.dictionary.BiWord;
import edu.ukma.fin.ir.atsaruk.search.indices.Index;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class BiWordSearchSubCommandService implements SubCommandService {

  private static final Logger LOGGER = LoggerFactory.getLogger(
      BiWordSearchSubCommandService.class);
  private static final String subCommandDescription = "Performs biword search within biword index";

  private final Searcher<String, BiWord, String> searcher;
  private final Index<BiWord, String> index;

  public BiWordSearchSubCommandService(
      final Searcher<String, BiWord, String> biWordSearcher,
      final Index<BiWord, String> biWordIndex) {
    this.searcher = biWordSearcher;
    this.index = biWordIndex;
  }

  @Override
  public Boolean apply(SubCommand subCommand) {
    LOGGER.debug("In search biword sub command service");

    final String arguments = subCommand.arguments();
    if (arguments.isEmpty()) {
      throw new CliException("Empty search arguments");
    }
    final List<String> searchResults = searcher.search(index, arguments);

    printToConsole(searchResults);

    return true;
  }

  @Override
  public String subCommandName() {
    return BIWORD_SUB_COMMAND;
  }

  @Override
  public String subCommandDescription() {
    return subCommandDescription;
  }

  @Override
  public String subCommandInfo() {
    return format("\t%s\t\t%s%n", subCommandName(), subCommandDescription());
  }
}
