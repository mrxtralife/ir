package edu.ukma.fin.ir.atsaruk.dictionary;

import static java.util.Collections.emptyList;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * Basic tokenizer for English words Splits text into words using regular expression and converts to
 * lowercase
 */
@Component
public class SimpleEnglishWordTokenizer implements Tokenizer<String> {

  private final static String TOKEN_REGEXP = "\\W+";

  @Override
  public List<String> tokenize(String text) {
    if (text == null || text.isEmpty()) {
      return emptyList();
    }

    final String[] tokens = text.split(TOKEN_REGEXP);
    return Arrays.stream(tokens)
        .filter(str -> !str.isEmpty())
        .collect(Collectors.toList());
  }
}
