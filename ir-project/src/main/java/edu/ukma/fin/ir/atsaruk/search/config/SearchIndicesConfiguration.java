package edu.ukma.fin.ir.atsaruk.search.config;

import edu.ukma.fin.ir.atsaruk.dictionary.BiWord;
import edu.ukma.fin.ir.atsaruk.search.indices.Index;
import java.util.Map;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SearchIndicesConfiguration {

  public SearchIndicesConfiguration(
      final Map<String, Index> searchIndices,
      final Index<String, String> invertedIndex,
      final Index<String, String> incidenceMatrix,
      final Index<BiWord, String> biWordIndex) {
    searchIndices.put("invertedIndex", invertedIndex);
    searchIndices.put("incidenceMatrix", incidenceMatrix);
    searchIndices.put("biWordIndex", biWordIndex);
  }
}
