package edu.ukma.fin.ir.atsaruk.cli.config;

import edu.ukma.fin.ir.atsaruk.cli.entity.Command;
import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import edu.ukma.fin.ir.atsaruk.cli.service.SubCommandService;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CliConfiguration {

  @Bean
  public Map<Predicate<Command>, Function<Command, Boolean>> commandHandlers() {
    return new HashMap<>();
  }

  /**
   * Holds Map of command services with service command name as key and command service info as
   * value
   *
   * @return map with command services
   */
  @Bean
  public Map<String, String> commands() {
    return new HashMap<>();
  }

  @Bean
  public Map<Predicate<SubCommand>, SubCommandService> loadDirSubCommandHandlers() {
    return new HashMap<>();
  }

  @Bean
  public Map<Predicate<SubCommand>, SubCommandService> createIndexSubCommandHandlers() {
    return new HashMap<>();
  }

  @Bean
  public Map<Predicate<SubCommand>, SubCommandService> simpleSearchSubCommandHandlers() {
    return new HashMap<>();
  }

  @Bean
  public Map<Predicate<SubCommand>, SubCommandService> searchSubCommandHandlers() {
    return new HashMap<>();
  }
}
