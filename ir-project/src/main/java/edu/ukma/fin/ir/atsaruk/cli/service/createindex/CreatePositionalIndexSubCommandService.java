package edu.ukma.fin.ir.atsaruk.cli.service.createindex;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.POSITIONAL_SUB_COMMAND;
import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import edu.ukma.fin.ir.atsaruk.cli.service.SubCommandService;
import edu.ukma.fin.ir.atsaruk.dictionary.Dictionary;
import edu.ukma.fin.ir.atsaruk.dictionary.DictionaryCatalog;
import edu.ukma.fin.ir.atsaruk.dictionary.PositionalWord;
import edu.ukma.fin.ir.atsaruk.search.indices.PositionalIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CreatePositionalIndexSubCommandService implements SubCommandService {

  private static final Logger LOGGER = LoggerFactory.getLogger(
      CreatePositionalIndexSubCommandService.class);
  private static final String subCommandDescription = "Creates positional index based on positional dictionary catalog";

  private final DictionaryCatalog<String, PositionalWord> catalog;
  private final PositionalIndex index;

  public CreatePositionalIndexSubCommandService(
      final DictionaryCatalog<String, PositionalWord> dictionaryCatalog,
      final PositionalIndex positionalIndex) {
    this.catalog = dictionaryCatalog;
    this.index = positionalIndex;
  }

  @Override
  public Boolean apply(SubCommand subCommand) {
    LOGGER.debug("In create-index positional sub command service");

    catalog.getNames().forEach(name -> {
      final Dictionary<PositionalWord> dictionary = catalog.get(name);
      dictionary.getTokens().forEach(token -> index.add(token, name));
    });

    return true;
  }

  @Override
  public String subCommandName() {
    return POSITIONAL_SUB_COMMAND;
  }

  @Override
  public String subCommandDescription() {
    return subCommandDescription;
  }

  @Override
  public String subCommandInfo() {
    return format("\t%s\t\t%s%n", subCommandName(), subCommandDescription());
  }
}
