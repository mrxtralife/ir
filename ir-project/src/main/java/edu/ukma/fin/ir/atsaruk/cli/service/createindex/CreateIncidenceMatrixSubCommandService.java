package edu.ukma.fin.ir.atsaruk.cli.service.createindex;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.INCIDENCE_MATRIX_SUB_COMMAND;
import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import edu.ukma.fin.ir.atsaruk.cli.service.SubCommandService;
import edu.ukma.fin.ir.atsaruk.dictionary.Dictionary;
import edu.ukma.fin.ir.atsaruk.dictionary.DictionaryCatalog;
import edu.ukma.fin.ir.atsaruk.search.indices.Index;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CreateIncidenceMatrixSubCommandService implements SubCommandService {

  private static final Logger LOGGER = LoggerFactory.getLogger(
      CreateIncidenceMatrixSubCommandService.class);
  private static final String subCommandDescription = "Creates incidence matrix based on string dictionary catalog";

  private final DictionaryCatalog<String, String> catalog;
  private final Index<String, String> incidenceMatrix;

  public CreateIncidenceMatrixSubCommandService(
      final DictionaryCatalog<String, String> dictionaryCatalog,
      final Index<String, String> incidenceMatrix) {
    this.catalog = dictionaryCatalog;
    this.incidenceMatrix = incidenceMatrix;
  }

  @Override
  public Boolean apply(SubCommand subCommand) {
    LOGGER.debug("In create-index incidence-matrix sub command service");

    catalog.getNames().forEach(name -> {
      final Dictionary<String> dictionary = catalog.get(name);
      dictionary.getTokens().forEach(token -> incidenceMatrix.add(token, name));
    });

    return true;
  }

  @Override
  public String subCommandName() {
    return INCIDENCE_MATRIX_SUB_COMMAND;
  }

  @Override
  public String subCommandDescription() {
    return subCommandDescription;
  }

  @Override
  public String subCommandInfo() {
    return format("\t%s\t\t%s%n", subCommandName(), subCommandDescription());
  }
}
