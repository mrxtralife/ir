package edu.ukma.fin.ir.atsaruk.dictionary;

import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class StringDictionaryCatalog implements DictionaryCatalog<String, String> {

  private final Map<String, Dictionary<String>> catalog = new HashMap<>();

  @Override
  public void add(final String name, final Dictionary<String> dictionary) {
    catalog.put(name, dictionary);
  }

  @Override
  public void remove(final String name) {
    catalog.remove(name);
  }

  @Override
  public Dictionary<String> get(final String name) {
    return catalog.get(name);
  }

  @Override
  public List<String> getNames() {
    return unmodifiableList(new ArrayList<>(catalog.keySet()));
  }
}
