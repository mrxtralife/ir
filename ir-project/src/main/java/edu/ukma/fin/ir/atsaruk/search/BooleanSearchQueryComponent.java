package edu.ukma.fin.ir.atsaruk.search;

import static edu.ukma.fin.ir.atsaruk.search.BooleanSearchOperator.AND;
import static edu.ukma.fin.ir.atsaruk.search.BooleanSearchOperator.OR;

public class BooleanSearchQueryComponent {

  private final BooleanSearchOperator operator;
  private final String searchTerm;
  private final boolean negate;

  private BooleanSearchQueryComponent(final BooleanSearchOperator operator,
      final String searchTerm,
      final boolean negate) {
    this.operator = operator;
    this.searchTerm = searchTerm;
    this.negate = negate;
  }

  public BooleanSearchOperator getOperator() {
    return operator;
  }

  public String getSearchTerm() {
    return searchTerm;
  }

  public boolean isNegate() {
    return negate;
  }

  public static class Builder {

    private BooleanSearchOperator operator;
    private String searchTerm;
    private boolean negate = false;

    private Builder(final BooleanSearchOperator operator) {
      this.operator = operator;
    }

    public Builder withSearchTerm(final String searchTerm) {
      this.searchTerm = searchTerm;
      return this;
    }

    public Builder negate() {
      this.negate = true;
      return this;
    }

    public BooleanSearchQueryComponent build() {
      return new BooleanSearchQueryComponent(operator, searchTerm, negate);
    }
  }

  public static Builder OR() {
    return new Builder(OR);
  }

  public static Builder AND() {
    return new Builder(AND);
  }
}
