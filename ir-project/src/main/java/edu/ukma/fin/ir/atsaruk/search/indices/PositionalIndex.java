package edu.ukma.fin.ir.atsaruk.search.indices;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;

import edu.ukma.fin.ir.atsaruk.dictionary.PositionalWord;
import edu.ukma.fin.ir.atsaruk.search.SearchUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class PositionalIndex {

  private final Map<String, Map<String, List<Long>>> index = new TreeMap<>();
  private final Set<String> values = new TreeSet<>();

  public void add(PositionalWord key, String value) {
    final Map<String, List<Long>> currentValues = getOrEmpty(key.word());
    List<Long> positions = currentValues.get(value);
    if (positions == null) {
      positions = new ArrayList<>();
    }
    positions.add(key.position());
    currentValues.put(value, positions);
    index.put(key.word(), currentValues);
    this.values.add(value);
  }

  public List<String> search(List<String> searchTerms, long distance) {
    final Set<String> documents = searchTerms.stream()
        .map(searchTerm -> index.get(searchTerm).keySet())
        .reduce(SearchUtils::intersectSets).orElse(emptySet());
    if (documents.isEmpty()) {
      return emptyList();
    }

    final List<String> resultingList = new ArrayList<>();
    documents.forEach(document -> {
      final List<List<Long>> positionList = new ArrayList<>();
      searchTerms.forEach(searchTerm -> positionList.add(index.get(searchTerm).get(document)));
      final List<Long> finalList = positionList.stream()
          .reduce((first, second) -> intersectWithDistance(first, second, distance))
          .orElse(emptyList());
      if (!finalList.isEmpty()) {
        resultingList.add(document);
      }
    });
    return resultingList;
  }

  private List<Long> intersectWithDistance(final List<Long> first, final List<Long> second,
      long distance) {
    if (first.isEmpty() || second.isEmpty()) {
      return emptyList();
    }
    final List<Long> resultingList = new ArrayList<>();
    first.forEach(firstPos -> {
      List<Long> withinDistance = second.stream()
          .filter(
              secondPos -> (secondPos <= firstPos + distance) && (secondPos >= firstPos - distance))
          .collect(Collectors.toList());
      if (!withinDistance.isEmpty()) {
        resultingList.add(firstPos);
        resultingList.addAll(withinDistance);
      }
    });
    return resultingList;
  }

  private Map<String, List<Long>> getOrEmpty(final String key) {
    final Map<String, List<Long>> values = index.get(key);
    return values != null ? values : new TreeMap<>();
  }
}
