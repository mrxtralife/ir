package edu.ukma.fin.ir.atsaruk.dictionary;

import edu.ukma.fin.ir.atsaruk.common.IrException;

/**
 * Exception for Dictionary Module
 */
public class DictionaryException extends IrException {

  public DictionaryException(String message) {
    super(message);
  }

  public DictionaryException(String message, Throwable cause) {
    super(message, cause);
  }

  public DictionaryException(Throwable cause) {
    super(cause);
  }
}
