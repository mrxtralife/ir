package edu.ukma.fin.ir.atsaruk.search.config;

import edu.ukma.fin.ir.atsaruk.search.indices.Index;
import java.util.HashMap;
import java.util.Map;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SearchConfiguration {

  @Bean
  public Map<String, Index> searchIndices() {
    return new HashMap<>();
  }
}
