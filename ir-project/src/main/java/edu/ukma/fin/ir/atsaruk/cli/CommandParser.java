package edu.ukma.fin.ir.atsaruk.cli;

import edu.ukma.fin.ir.atsaruk.cli.entity.Command;
import edu.ukma.fin.ir.atsaruk.cli.entity.ImmutableCommand;
import edu.ukma.fin.ir.atsaruk.common.Parser;
import org.springframework.stereotype.Component;

@Component
public class CommandParser implements Parser<String, Command> {

  @Override
  public Command parse(String source) {
    assert source != null;
    final String[] parsedSource = source.split("\\s+", 3);

    final String command = parsedSource[0];
    final String subCommand = parsedSource.length > 1 ? parsedSource[1] : "";
    final String arguments = parsedSource.length > 2 ? parsedSource[2] : "";

    return ImmutableCommand.builder()
        .command(command)
        .subCommand(subCommand)
        .arguments(arguments)
        .build();
  }
}
