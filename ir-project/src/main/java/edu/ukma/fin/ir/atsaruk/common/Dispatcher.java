package edu.ukma.fin.ir.atsaruk.common;

/**
 * Simple interface for dispatching data
 *
 * @param <T> defines type of data to be dispatched
 */
public interface Dispatcher<T> {

  /**
   * Dispatcher function
   *
   * @param data data to be dispatched
   * @return true if data dispatched successfully and false otherwise
   */
  boolean dispatch(final T data);

}
