package edu.ukma.fin.ir.atsaruk.dictionary;

import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class BiWordDictionaryCatalog implements DictionaryCatalog<String, BiWord> {

  private final Map<String, Dictionary<BiWord>> catalog = new HashMap<>();

  @Override
  public void add(final String name, final Dictionary<BiWord> dictionary) {
    catalog.put(name, dictionary);
  }

  @Override
  public void remove(final String name) {
    catalog.remove(name);
  }

  @Override
  public Dictionary<BiWord> get(final String name) {
    return catalog.get(name);
  }

  @Override
  public List<String> getNames() {
    return unmodifiableList(new ArrayList<>(catalog.keySet()));
  }
}
