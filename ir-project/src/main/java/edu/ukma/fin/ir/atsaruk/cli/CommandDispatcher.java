package edu.ukma.fin.ir.atsaruk.cli;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.valueOrEmpty;
import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.entity.Command;
import edu.ukma.fin.ir.atsaruk.common.Dispatcher;
import edu.ukma.fin.ir.atsaruk.common.IrException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.function.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CommandDispatcher implements Dispatcher<Command> {

  private static Logger LOGGER = LoggerFactory.getLogger(CommandDispatcher.class);

  private final Map<Predicate<Command>, Function<Command, Boolean>> handlers;

  public CommandDispatcher(
      Map<Predicate<Command>, Function<Command, Boolean>> handlers) {
    this.handlers = handlers;
  }

  @Override
  public boolean dispatch(Command command) {
    for (Entry<Predicate<Command>, Function<Command, Boolean>> entry : handlers.entrySet()) {
      if (entry.getKey().test(command)) {
        final Function<Command, Boolean> handler = entry.getValue();
        assert handler != null;
        LOGGER.debug("The handler {} is found and will process the command -> {} {}",
            handler.getClass().getName(), command.command(), valueOrEmpty(command.subCommand()));

        try {
          return handler.apply(command);
        } catch (IrException ex) {
          throw new CliException(ex.getMessage(), ex);
        }
      }
    }
    throw new CliException(
        format("Need to describe rule and assign handler for the command -> %s %s",
            command.command(), valueOrEmpty(command.subCommand())));
  }
}
