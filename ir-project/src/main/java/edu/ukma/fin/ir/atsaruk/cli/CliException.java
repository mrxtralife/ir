package edu.ukma.fin.ir.atsaruk.cli;

import edu.ukma.fin.ir.atsaruk.common.IrException;

/**
 * Exception for Cli Module
 */
public class CliException extends IrException {

  public CliException(String message) {
    super(message);
  }

  public CliException(String message, Throwable cause) {
    super(message, cause);
  }

  public CliException(Throwable cause) {
    super(cause);
  }
}
