package edu.ukma.fin.ir.atsaruk.cli.service;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.CREATE_INDEX_COMMAND;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.EMPTY_STRING;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.EXIT_COMMAND;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.HELP_COMMAND;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.LOAD_DIRECTORY_COMMAND;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.SEARCH_COMMAND;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.SIMPLE_SEARCH_COMMAND;

import edu.ukma.fin.ir.atsaruk.cli.entity.Command;
import java.util.function.Predicate;

public enum CommandRule implements Predicate<Command> {
  EMPTY_COMMAND_RULE(command -> EMPTY_STRING.equals(command.command())),

  HELP_COMMAND_RULE(command -> HELP_COMMAND.equals(command.command())),

  EXIT_COMMAND_RULE(command -> EXIT_COMMAND.equals(command.command())),

  LOAD_DIRECTORY_COMMAND_RULE(command -> LOAD_DIRECTORY_COMMAND.equals(command.command())),

  CREATE_INDEX_COMMAND_RULE(command -> CREATE_INDEX_COMMAND.equals(command.command())),

  SIMPLE_SEARCH_COMMAND_RULE(command -> SIMPLE_SEARCH_COMMAND.equals(command.command())),

  SEARCH_COMMAND_RULE(command -> SEARCH_COMMAND.equals(command.command()));

  private final Predicate<Command> rule;

  CommandRule(Predicate<Command> rule) {
    this.rule = rule;
  }

  @Override
  public boolean test(Command command) {
    return rule.test(command);
  }
}
