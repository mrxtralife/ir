package edu.ukma.fin.ir.atsaruk.search;

import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.common.Parser;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class BooleanSearchArgumentParser implements Parser<String, BooleanSearchQuery> {

  private final static String OR_STRING = "OR";
  private final static String AND_STRING = "AND";

  @Override
  public BooleanSearchQuery parse(final String searchString) {
    if (searchString == null || searchString.isEmpty()) {
      throw new SearchException("Search string is null or empty");
    }

    BooleanSearchQuery query = new BooleanSearchQuery();
    processSearchString(OR_STRING + " " + searchString, query);

    return query;
  }

  private void processSearchString(final String searchString, final BooleanSearchQuery query) {
    final String[] operatorAndTail = searchString.split("\\s+", 2);
    final String operator = operatorAndTail[0];
    final String tail = operatorAndTail[1];

    final BooleanSearchQueryComponent.Builder builder;
    if (OR_STRING.equals(operator)) {
      builder = BooleanSearchQueryComponent.OR();
    } else if (AND_STRING.equals(operator)) {
      builder = BooleanSearchQueryComponent.AND();
    } else {
      throw new SearchException(
          format("Error while parsing binary search query. Expected 'OR' or 'AND'. Was '%s'",
              operator));
    }

    final String searchTerm;
    final String otherSearchString;

    Pattern pattern = Pattern.compile("\\bOR|AND\\b");
    Matcher matcher = pattern.matcher(tail);

    if (matcher.find()) {
      final int index = matcher.start();
      searchTerm = tail.substring(0, index).trim();
      otherSearchString = tail.substring(index).trim();
    } else {
      searchTerm = tail;
      otherSearchString = null;
    }

    if (searchTerm.matches("^NOT\\b.*")) {
      String negatedSearchTerm = searchTerm.substring(3).trim();
      query.addComponent(builder.withSearchTerm(negatedSearchTerm).negate().build());
    } else {
      query.addComponent(builder.withSearchTerm(searchTerm).build());
    }

    if (otherSearchString != null) {
      processSearchString(otherSearchString, query);
    }
  }
}
