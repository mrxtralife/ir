package edu.ukma.fin.ir.atsaruk.cli.service;

import edu.ukma.fin.ir.atsaruk.cli.entity.Command;
import java.util.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class EmptyCommandService implements Function<Command, Boolean> {

  private static final Logger LOGGER = LoggerFactory.getLogger(EmptyCommandService.class);

  @Override
  public Boolean apply(Command command) {
    LOGGER.debug("In empty command service");

    return true;
  }
}
