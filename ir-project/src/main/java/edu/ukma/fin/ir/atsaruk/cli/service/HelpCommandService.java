package edu.ukma.fin.ir.atsaruk.cli.service;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.HELP_COMMAND;
import static java.lang.String.format;
import static java.util.Collections.emptyList;

import edu.ukma.fin.ir.atsaruk.cli.entity.Command;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class HelpCommandService implements CommandService {

  private static final Logger LOGGER = LoggerFactory.getLogger(HelpCommandService.class);
  private static final String commandDescription = "Prints list of supported commands";

  private Map<String, String> commands;

  public HelpCommandService(Map<String, String> commands) {
    this.commands = commands;
  }

  @Override
  public Boolean apply(Command command) {
    LOGGER.debug("In help command service");

    commands.values().stream()
        .sorted()
        .forEachOrdered(System.out::println);

    return true;
  }

  @Override
  public String commandName() {
    return HELP_COMMAND;
  }

  @Override
  public String commandDescription() {
    return commandDescription;
  }

  @Override
  public List<String> subCommandList() {
    return emptyList();
  }

  @Override
  public String commandInfo() {
    return format("%s\t\t%s%n", commandName(), commandDescription());
  }
}
