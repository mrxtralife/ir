package edu.ukma.fin.ir.atsaruk.dictionary;

import static java.util.Collections.unmodifiableCollection;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

class PositionalWordDictionary implements Dictionary<PositionalWord> {

  private final Set<PositionalWord> dictionary = new TreeSet<>();

  @Override
  public void add(final PositionalWord token) {
    dictionary.add(token);
  }

  @Override
  public void add(final Collection<PositionalWord> tokens) {
    dictionary.addAll(tokens);
  }

  @Override
  public void remove(final PositionalWord token) {
    dictionary.remove(token);
  }

  @Override
  public void remove(final Collection<PositionalWord> tokens) {
    dictionary.removeAll(tokens);
  }

  @Override
  public Collection<PositionalWord> getTokens() {
    return unmodifiableCollection(dictionary);
  }
}
