package edu.ukma.fin.ir.atsaruk.search;

import edu.ukma.fin.ir.atsaruk.common.IrException;

/**
 * Exception for Search Module
 */
public class SearchException extends IrException {

  public SearchException(String message) {
    super(message);
  }

  public SearchException(String message, Throwable cause) {
    super(message, cause);
  }

  public SearchException(Throwable cause) {
    super(cause);
  }
}
