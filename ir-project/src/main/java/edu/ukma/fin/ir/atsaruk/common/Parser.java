package edu.ukma.fin.ir.atsaruk.common;

public interface Parser<S, R> {

  R parse(final S source);
}
