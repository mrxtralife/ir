package edu.ukma.fin.ir.atsaruk.search;

import static edu.ukma.fin.ir.atsaruk.search.BooleanSearchOperator.AND;
import static edu.ukma.fin.ir.atsaruk.search.BooleanSearchOperator.OR;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class SearchUtils {

  private SearchUtils() {
    //Util class
  }

  public static <T> List<T> combineLists(final List<T> base, final List<T> addition,
      final BooleanSearchOperator operator) {
    if (operator == OR) {
      return unionLists(base, addition);
    }
    if (operator == AND) {
      return intersectLists(base, addition);
    }
    throw new SearchException("Empty or unknown search operator");
  }

  public static <T> List<T> unionLists(final List<T> base, final List<T> addition) {
    final List<T> resultingList = new ArrayList<>(base);
    addition.forEach(item -> {
      if (!resultingList.contains(item)) {
        resultingList.add(item);
      }
    });
    return resultingList;
  }

  public static <T> List<T> intersectLists(final List<T> base, final List<T> addition) {
    final List<T> resultingList = new ArrayList<>();
    base.forEach(item -> {
      if (addition.contains(item)) {
        resultingList.add(item);
      }
    });

    return resultingList;
  }

  public static <T> Set<T> intersectSets(final Set<T> base, final Set<T> addition) {
    final Set<T> resultingSet = new HashSet<>();
    base.forEach(item -> {
      if (addition.contains(item)) {
        resultingSet.add(item);
      }
    });

    return resultingSet;
  }
}
