package edu.ukma.fin.ir.atsaruk.common;

/**
 * Basic Information Retrieval Exception
 */
public class IrException extends RuntimeException {

  public IrException(final String message) {
    super(message);
  }

  public IrException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public IrException(final Throwable cause) {
    super(cause);
  }
}
