package edu.ukma.fin.ir.atsaruk.dictionary;

import java.util.List;

public interface DictionaryCatalog<N, T> {

  void add(final N name, final Dictionary<T> dictionary);

  void remove(final N name);

  Dictionary<T> get(final N name);

  List<N> getNames();
}
