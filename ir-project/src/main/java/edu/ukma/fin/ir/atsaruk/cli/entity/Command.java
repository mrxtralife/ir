package edu.ukma.fin.ir.atsaruk.cli.entity;

import org.immutables.value.Value.Immutable;

@Immutable
public interface Command {

  String command();

  String subCommand();

  String arguments();
}
