package edu.ukma.fin.ir.atsaruk.search;

import static edu.ukma.fin.ir.atsaruk.dictionary.PostProcessorUtils.asPipe;
import static edu.ukma.fin.ir.atsaruk.search.SearchUtils.combineLists;

import edu.ukma.fin.ir.atsaruk.common.Parser;
import edu.ukma.fin.ir.atsaruk.dictionary.PostProcessor;
import edu.ukma.fin.ir.atsaruk.search.indices.Searchable;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class BooleanSearcher implements Searcher<String, String, String> {

  private final Parser<String, BooleanSearchQuery> parser;
  private final List<PostProcessor<String>> postProcessors;

  public BooleanSearcher(final Parser<String, BooleanSearchQuery> parser,
      final List<PostProcessor<String>> postProcessors) {
    this.parser = parser;
    this.postProcessors = postProcessors;
  }

  @Override
  public List<String> search(Searchable<String, String> source, String searchString) {
    final BooleanSearchQuery query = parser.parse(searchString);
    List<String> queryResult = new ArrayList<>();
    for (BooleanSearchQueryComponent component : query.getComponents()) {
      final String searchTerm = component.getSearchTerm();
      final List<String> componentResult = component.isNegate()
          ? source.searchNot(asPipe(postProcessors).apply(searchTerm))
          : source.search(asPipe(postProcessors).apply(searchTerm));

      queryResult = combineLists(queryResult, componentResult, component.getOperator());
    }

    return queryResult;
  }
}
