package edu.ukma.fin.ir.atsaruk.cli.service.createindex;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.INVERTED_INDEX_SUB_COMMAND;
import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import edu.ukma.fin.ir.atsaruk.cli.service.SubCommandService;
import edu.ukma.fin.ir.atsaruk.dictionary.Dictionary;
import edu.ukma.fin.ir.atsaruk.dictionary.DictionaryCatalog;
import edu.ukma.fin.ir.atsaruk.search.indices.Index;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CreateInvertedIndexSubCommandService implements SubCommandService {

  private static final Logger LOGGER = LoggerFactory.getLogger(
      CreateInvertedIndexSubCommandService.class);
  private static final String subCommandDescription = "Creates inverted index based on string dictionary catalog";

  private final DictionaryCatalog<String, String> catalog;
  private final Index<String, String> invertedIndex;

  public CreateInvertedIndexSubCommandService(
      final DictionaryCatalog<String, String> dictionaryCatalog,
      final Index<String, String> invertedIndex) {
    this.catalog = dictionaryCatalog;
    this.invertedIndex = invertedIndex;
  }

  @Override
  public Boolean apply(SubCommand subCommand) {
    LOGGER.debug("In create-index inverted-index sub command service");

    catalog.getNames().forEach(name -> {
      final Dictionary<String> dictionary = catalog.get(name);
      dictionary.getTokens().forEach(token -> invertedIndex.add(token, name));
    });

    return true;
  }

  @Override
  public String subCommandName() {
    return INVERTED_INDEX_SUB_COMMAND;
  }

  @Override
  public String subCommandDescription() {
    return subCommandDescription;
  }

  @Override
  public String subCommandInfo() {
    return format("\t%s\t\t%s%n", subCommandName(), subCommandDescription());
  }
}
