package edu.ukma.fin.ir.atsaruk.cli;

import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import edu.ukma.fin.ir.atsaruk.cli.service.SubCommandService;
import edu.ukma.fin.ir.atsaruk.common.Dispatcher;
import edu.ukma.fin.ir.atsaruk.common.IrException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubCommandDispatcher implements Dispatcher<SubCommand> {

  private final static Logger LOGGER = LoggerFactory.getLogger(SubCommandDispatcher.class);

  private final Map<Predicate<SubCommand>, SubCommandService> handlers;

  public SubCommandDispatcher(final Map<Predicate<SubCommand>, SubCommandService> handlers) {
    this.handlers = handlers;
  }

  @Override
  public boolean dispatch(final SubCommand subCommand) {
    for (Entry<Predicate<SubCommand>, SubCommandService> entry : handlers.entrySet()) {
      if (entry.getKey().test(subCommand)) {
        final SubCommandService handler = entry.getValue();
        assert handler != null;
        LOGGER.debug("The handler {} is found and will process the sub command -> {}",
            handler.getClass().getName(), subCommand.subCommand());

        try {
          return handler.apply(subCommand);
        } catch (IrException ex) {
          throw new CliException(ex.getMessage(), ex);
        }
      }
    }
    throw new CliException(
        format("Need to describe rule and assign handler for the sub command -> %s",
            subCommand.subCommand()));
  }
}
