package edu.ukma.fin.ir.atsaruk.dictionary;

import static edu.ukma.fin.ir.atsaruk.dictionary.PostProcessorUtils.asPipe;
import static java.lang.String.format;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class PlainEnglishTextFileBiWordDictionaryGenerator implements
    DictionaryGenerator<Path, BiWord> {

  private final Tokenizer<String> tokenizer;
  private final List<PostProcessor<String>> postProcessors;

  public PlainEnglishTextFileBiWordDictionaryGenerator(final Tokenizer<String> tokenizer,
      final List<PostProcessor<String>> postProcessors) {
    this.tokenizer = tokenizer;
    this.postProcessors = postProcessors;
  }

  @Override
  public Dictionary<BiWord> generate(Path source) {
    if (source == null ||
        Files.notExists(source) ||
        !Files.isRegularFile(source) ||
        !Files.isReadable(source)) {
      throw new DictionaryException(format("Invalid source path: %s", source));
    }

    Dictionary<BiWord> dictionary = Dictionaries.getBiWordDictionary();
    try {
      Files.lines(source)
          .map(tokenizer)
          .flatMap(Collection::stream)
          .map(asPipe(postProcessors))
          .filter(str -> !str.isEmpty())
          .reduce((first, second) -> {
            final BiWord biWord = ImmutableBiWord.builder().first(first).second(second).build();
            dictionary.add(biWord);
            return second;
          });
    } catch (IOException ex) {
      throw new DictionaryException(format("Error while reading source path: %s", source), ex);
    }

    return dictionary;
  }
}
