package edu.ukma.fin.ir.atsaruk.search;

import edu.ukma.fin.ir.atsaruk.search.indices.Searchable;
import java.util.List;
import java.util.function.BiFunction;

public interface Searcher<Q, I, R> extends BiFunction<Searchable<I, R>, Q, List<R>> {

  List<R> search(final Searchable<I, R> source, final Q query);

  @Override
  default List<R> apply(Searchable<I, R> source, Q query) {
    return search(source, query);
  }
}
