package edu.ukma.fin.ir.atsaruk.cli.config;

import static edu.ukma.fin.ir.atsaruk.cli.service.CommandRule.CREATE_INDEX_COMMAND_RULE;
import static edu.ukma.fin.ir.atsaruk.cli.service.CommandRule.EMPTY_COMMAND_RULE;
import static edu.ukma.fin.ir.atsaruk.cli.service.CommandRule.EXIT_COMMAND_RULE;
import static edu.ukma.fin.ir.atsaruk.cli.service.CommandRule.HELP_COMMAND_RULE;
import static edu.ukma.fin.ir.atsaruk.cli.service.CommandRule.LOAD_DIRECTORY_COMMAND_RULE;
import static edu.ukma.fin.ir.atsaruk.cli.service.CommandRule.SEARCH_COMMAND_RULE;
import static edu.ukma.fin.ir.atsaruk.cli.service.CommandRule.SIMPLE_SEARCH_COMMAND_RULE;

import edu.ukma.fin.ir.atsaruk.cli.entity.Command;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommandHandlersConfiguration {

  public CommandHandlersConfiguration(
      Map<Predicate<Command>, Function<Command, Boolean>> commandHandlers,
      Function<Command, Boolean> emptyCommandService,
      Function<Command, Boolean> helpCommandService,
      Function<Command, Boolean> exitCommandService,
      Function<Command, Boolean> loadDirCommandService,
      Function<Command, Boolean> createIndexCommandService,
      Function<Command, Boolean> simpleSearchCommandService,
      Function<Command, Boolean> searchCommandService
  ) {

    commandHandlers.put(EMPTY_COMMAND_RULE, emptyCommandService);
    commandHandlers.put(HELP_COMMAND_RULE, helpCommandService);
    commandHandlers.put(EXIT_COMMAND_RULE, exitCommandService);
    commandHandlers.put(LOAD_DIRECTORY_COMMAND_RULE, loadDirCommandService);
    commandHandlers.put(CREATE_INDEX_COMMAND_RULE, createIndexCommandService);
    commandHandlers.put(SIMPLE_SEARCH_COMMAND_RULE, simpleSearchCommandService);
    commandHandlers.put(SEARCH_COMMAND_RULE, searchCommandService);
  }
}
