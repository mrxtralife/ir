package edu.ukma.fin.ir.atsaruk.cli.service.simplesearch;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.INCIDENCE_MATRIX_SUB_COMMAND;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.printToConsole;
import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.CliException;
import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import edu.ukma.fin.ir.atsaruk.cli.service.SubCommandService;
import edu.ukma.fin.ir.atsaruk.search.Searcher;
import edu.ukma.fin.ir.atsaruk.search.indices.Index;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SimpleSearchIncidenceMatrixSubCommandService implements SubCommandService {

  private static final Logger LOGGER = LoggerFactory.getLogger(
      SimpleSearchIncidenceMatrixSubCommandService.class);
  private static final String subCommandDescription = "Performs search of given single term within incidence matrix";

  private final Searcher<String, String, String> searcher;
  private final Index<String, String> index;

  public SimpleSearchIncidenceMatrixSubCommandService(
      final Searcher<String, String, String> simpleSearcher,
      final Index<String, String> incidenceMatrix) {
    this.searcher = simpleSearcher;
    this.index = incidenceMatrix;
  }

  @Override
  public Boolean apply(SubCommand subCommand) {
    LOGGER.debug("In simple-search incidence-matrix sub command service");

    final String arguments = subCommand.arguments();
    if (arguments.isEmpty()) {
      throw new CliException("Empty search arguments");
    }
    final List<String> searchResults = searcher.search(index, arguments);

    printToConsole(searchResults);

    return true;
  }

  @Override
  public String subCommandName() {
    return INCIDENCE_MATRIX_SUB_COMMAND;
  }

  @Override
  public String subCommandDescription() {
    return subCommandDescription;
  }

  @Override
  public String subCommandInfo() {
    return format("\t%s\t\t%s%n", subCommandName(), subCommandDescription());
  }
}
