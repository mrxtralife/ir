package edu.ukma.fin.ir.atsaruk.search;

import static edu.ukma.fin.ir.atsaruk.dictionary.PostProcessorUtils.asPipe;

import edu.ukma.fin.ir.atsaruk.dictionary.PostProcessor;
import edu.ukma.fin.ir.atsaruk.search.indices.Searchable;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class SimpleSearcher implements Searcher<String, String, String> {

  private final List<PostProcessor<String>> postProcessors;

  public SimpleSearcher(
      final List<PostProcessor<String>> postProcessors) {
    this.postProcessors = postProcessors;
  }

  @Override
  public List<String> search(Searchable<String, String> source, String query) {
    return source.search(asPipe(postProcessors).apply(query));
  }
}
