package edu.ukma.fin.ir.atsaruk.dictionary.service;

import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.CliException;
import edu.ukma.fin.ir.atsaruk.dictionary.Dictionary;
import edu.ukma.fin.ir.atsaruk.dictionary.DictionaryCatalog;
import edu.ukma.fin.ir.atsaruk.dictionary.DictionaryException;
import edu.ukma.fin.ir.atsaruk.dictionary.DictionaryGenerator;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.stereotype.Service;

@Service
public class StringDictionaryCatalogService {

  //TODO Move catalog to service parameters later
  private final DictionaryCatalog<String, String> catalog;
  private final DictionaryGenerator<Path, String> generator;

  public StringDictionaryCatalogService(
      final DictionaryCatalog<String, String> catalog,
      final DictionaryGenerator<Path, String> generator) {
    this.catalog = catalog;
    this.generator = generator;
  }

  public void loadDirectoryContent(final String directory) {
    final Path directoryPath = Paths.get(directory);
    if (!Files.isDirectory(directoryPath)) {
      throw new DictionaryException(format("Wrong directory: %s", directory));
    }

    try {
      Files.walk(directoryPath).forEach(path -> {
        if (Files.isRegularFile(path)) {
          final Dictionary<String> dictionary = generator.generate(path);
          catalog.add(path.toAbsolutePath().toString(), dictionary);
        }
      });
    } catch (IOException ex) {
      throw new CliException(ex.getMessage(), ex);
    }
  }
}
