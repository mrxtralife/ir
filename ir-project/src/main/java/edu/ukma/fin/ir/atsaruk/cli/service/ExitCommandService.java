package edu.ukma.fin.ir.atsaruk.cli.service;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.EXIT_COMMAND;
import static java.lang.String.format;
import static java.util.Collections.emptyList;

import edu.ukma.fin.ir.atsaruk.cli.entity.Command;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ExitCommandService implements CommandService {

  private static final Logger LOGGER = LoggerFactory.getLogger(ExitCommandService.class);
  private static final String commandDescription = "Exits command line interactive client";

  @Override
  public Boolean apply(Command command) {
    LOGGER.debug("In exit command service");

    return false;
  }

  @Override
  public String commandName() {
    return EXIT_COMMAND;
  }

  @Override
  public String commandDescription() {
    return commandDescription;
  }

  @Override
  public String commandInfo() {
    return format("%s\t\t%s%n", EXIT_COMMAND, commandDescription());
  }

  @Override
  public List<String> subCommandList() {
    return emptyList();
  }
}
