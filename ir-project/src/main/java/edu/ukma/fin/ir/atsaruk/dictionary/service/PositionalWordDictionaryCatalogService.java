package edu.ukma.fin.ir.atsaruk.dictionary.service;

import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.CliException;
import edu.ukma.fin.ir.atsaruk.dictionary.Dictionary;
import edu.ukma.fin.ir.atsaruk.dictionary.DictionaryCatalog;
import edu.ukma.fin.ir.atsaruk.dictionary.DictionaryException;
import edu.ukma.fin.ir.atsaruk.dictionary.DictionaryGenerator;
import edu.ukma.fin.ir.atsaruk.dictionary.PositionalWord;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.stereotype.Service;

@Service
public class PositionalWordDictionaryCatalogService {

  //TODO Move catalog to service parameters later
  private final DictionaryCatalog<String, PositionalWord> catalog;
  private final DictionaryGenerator<Path, PositionalWord> generator;

  public PositionalWordDictionaryCatalogService(
      final DictionaryCatalog<String, PositionalWord> catalog,
      final DictionaryGenerator<Path, PositionalWord> generator) {
    this.catalog = catalog;
    this.generator = generator;
  }

  public void loadDirectoryContent(final String directory) {
    final Path directoryPath = Paths.get(directory);
    if (!Files.isDirectory(directoryPath)) {
      throw new DictionaryException(format("Wrong directory: %s", directory));
    }

    try {
      Files.walk(directoryPath).forEach(path -> {
        if (Files.isRegularFile(path)) {
          final Dictionary<PositionalWord> dictionary = generator.generate(path);
          catalog.add(path.toAbsolutePath().toString(), dictionary);
        }
      });
    } catch (IOException ex) {
      throw new CliException(ex.getMessage(), ex);
    }
  }
}
