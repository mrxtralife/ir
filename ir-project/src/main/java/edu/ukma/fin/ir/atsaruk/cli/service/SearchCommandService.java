package edu.ukma.fin.ir.atsaruk.cli.service;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.SEARCH_COMMAND;
import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.SubCommandDispatcher;
import edu.ukma.fin.ir.atsaruk.cli.entity.Command;
import edu.ukma.fin.ir.atsaruk.cli.entity.ImmutableSubCommand;
import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import edu.ukma.fin.ir.atsaruk.common.Dispatcher;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SearchCommandService implements CommandService {

  private static final Logger LOGGER = LoggerFactory.getLogger(SearchCommandService.class);
  private static final String commandDescription = "Performs search of given search term within specified index";

  private final Dispatcher<SubCommand> dispatcher;
  private final Collection<SubCommandService> subCommandServices;

  public SearchCommandService(
      final Map<Predicate<SubCommand>, SubCommandService> searchSubCommandHandlers) {
    this.dispatcher = new SubCommandDispatcher(searchSubCommandHandlers);
    this.subCommandServices = searchSubCommandHandlers.values();
  }

  @Override
  public Boolean apply(Command command) {
    LOGGER.debug("In search command service");

    final SubCommand subCommand = ImmutableSubCommand.builder()
        .subCommand(command.subCommand())
        .arguments(command.arguments())
        .build();

    dispatcher.dispatch(subCommand);

    return true;
  }

  @Override
  public String commandName() {
    return SEARCH_COMMAND;
  }

  @Override
  public String commandDescription() {
    return commandDescription;
  }

  @Override
  public String commandInfo() {
    final StringBuilder builder = new StringBuilder(
        format("%s\t\t%s%n", commandName(), commandDescription()));
    subCommandServices.forEach(service -> builder.append(service.subCommandInfo()));

    return builder.toString();
  }

  @Override
  public List<String> subCommandList() {
    return subCommandServices.stream()
        .map(SubCommandService::subCommandName)
        .collect(Collectors.toList());
  }
}
