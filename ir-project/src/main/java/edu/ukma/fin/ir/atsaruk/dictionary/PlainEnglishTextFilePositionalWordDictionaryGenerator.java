package edu.ukma.fin.ir.atsaruk.dictionary;

import static edu.ukma.fin.ir.atsaruk.dictionary.PostProcessorUtils.asPipe;
import static java.lang.String.format;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.stereotype.Component;

@Component
public class PlainEnglishTextFilePositionalWordDictionaryGenerator implements
    DictionaryGenerator<Path, PositionalWord> {

  private final Tokenizer<String> tokenizer;
  private final List<PostProcessor<String>> postProcessors;

  public PlainEnglishTextFilePositionalWordDictionaryGenerator(final Tokenizer<String> tokenizer,
      final List<PostProcessor<String>> postProcessors) {
    this.tokenizer = tokenizer;
    this.postProcessors = postProcessors;
  }

  @Override
  public Dictionary<PositionalWord> generate(Path source) {
    if (source == null ||
        Files.notExists(source) ||
        !Files.isRegularFile(source) ||
        !Files.isReadable(source)) {
      throw new DictionaryException(format("Invalid source path: %s", source));
    }

    Dictionary<PositionalWord> dictionary = Dictionaries.getPositionalWordDictionary();
    try {
      AtomicLong index = new AtomicLong(1);
      Files.lines(source)
          .map(tokenizer)
          .flatMap(Collection::stream)
          .map(asPipe(postProcessors))
          .filter(str -> !str.isEmpty())
          .map(word -> ImmutablePositionalWord.builder()
              .word(word)
              .position(index.getAndIncrement())
              .build())
          .forEach(dictionary::add);
    } catch (IOException ex) {
      throw new DictionaryException(format("Error while reading source path: %s", source), ex);
    }

    return dictionary;
  }
}
