package edu.ukma.fin.ir.atsaruk;

import edu.ukma.fin.ir.atsaruk.cli.CommandLineInteractiveClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InformationRetrievalStarter implements CommandLineRunner {

  private static Logger LOGGER = LoggerFactory.getLogger(InformationRetrievalStarter.class);

  private final CommandLineInteractiveClient client;

  public InformationRetrievalStarter(CommandLineInteractiveClient client) {
    this.client = client;
  }

  public static void main(String[] args) {
    SpringApplication.run(InformationRetrievalStarter.class, args);
  }

  @Override
  public void run(String... args) {
    LOGGER.debug("Starting: Command line interactive client");

    client.interact();

    LOGGER.debug("Finished: Command line interactive client");
  }
}
