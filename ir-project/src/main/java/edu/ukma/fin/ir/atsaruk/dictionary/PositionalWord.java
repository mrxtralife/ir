package edu.ukma.fin.ir.atsaruk.dictionary;

import org.immutables.value.Value.Immutable;

@Immutable
public interface PositionalWord extends Comparable<PositionalWord> {

  String word();

  long position();

  @Override
  default int compareTo(final PositionalWord other) {
    int firstCompare = this.word().compareTo(other.word());
    if (firstCompare != 0) {
      return firstCompare;
    }
    return Long.compare(position(), other.position());
  }
}
