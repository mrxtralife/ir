package edu.ukma.fin.ir.atsaruk.dictionary;

import static java.util.Collections.unmodifiableCollection;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

class BiWordDictionary implements Dictionary<BiWord> {

  private final Set<BiWord> dictionary = new TreeSet<>();

  @Override
  public void add(final BiWord token) {
    dictionary.add(token);
  }

  @Override
  public void add(final Collection<BiWord> tokens) {
    dictionary.addAll(tokens);
  }

  @Override
  public void remove(final BiWord token) {
    dictionary.remove(token);
  }

  @Override
  public void remove(final Collection<BiWord> tokens) {
    dictionary.removeAll(tokens);
  }

  @Override
  public Collection<BiWord> getTokens() {
    return unmodifiableCollection(dictionary);
  }
}
