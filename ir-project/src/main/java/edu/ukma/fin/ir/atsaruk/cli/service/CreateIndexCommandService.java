package edu.ukma.fin.ir.atsaruk.cli.service;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.CREATE_INDEX_COMMAND;
import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.SubCommandDispatcher;
import edu.ukma.fin.ir.atsaruk.cli.entity.Command;
import edu.ukma.fin.ir.atsaruk.cli.entity.ImmutableSubCommand;
import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import edu.ukma.fin.ir.atsaruk.common.Dispatcher;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CreateIndexCommandService implements CommandService {

  private static final Logger LOGGER = LoggerFactory.getLogger(
      CreateIndexCommandService.class);
  private static final String commandDescription = "Creates specified index based on current dictionary catalog";

  private final Dispatcher<SubCommand> dispatcher;
  private final Collection<SubCommandService> subCommandServices;

  public CreateIndexCommandService(
      final Map<Predicate<SubCommand>, SubCommandService> createIndexSubCommandHandlers) {
    this.dispatcher = new SubCommandDispatcher(createIndexSubCommandHandlers);
    this.subCommandServices = createIndexSubCommandHandlers.values();
  }

  @Override
  public Boolean apply(Command command) {
    LOGGER.debug("In create-index command service");

    final SubCommand subCommand = ImmutableSubCommand.builder()
        .subCommand(command.subCommand())
        .arguments(command.arguments())
        .build();

    dispatcher.dispatch(subCommand);

    return true;
  }

  @Override
  public String commandName() {
    return CREATE_INDEX_COMMAND;
  }

  @Override
  public String commandDescription() {
    return commandDescription;
  }

  @Override
  public String commandInfo() {
    final StringBuilder builder = new StringBuilder(
        format("%s\t\t%s%n", commandName(), commandDescription()));
    subCommandServices.forEach(service -> builder.append(service.subCommandInfo()));

    return builder.toString();
  }

  @Override
  public List<String> subCommandList() {
    return subCommandServices.stream()
        .map(SubCommandService::subCommandName)
        .collect(Collectors.toList());
  }
}
