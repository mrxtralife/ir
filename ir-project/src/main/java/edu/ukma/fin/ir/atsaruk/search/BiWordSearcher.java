package edu.ukma.fin.ir.atsaruk.search;

import static edu.ukma.fin.ir.atsaruk.dictionary.PostProcessorUtils.asPipe;

import edu.ukma.fin.ir.atsaruk.dictionary.BiWord;
import edu.ukma.fin.ir.atsaruk.dictionary.ImmutableBiWord;
import edu.ukma.fin.ir.atsaruk.dictionary.PostProcessor;
import edu.ukma.fin.ir.atsaruk.dictionary.Tokenizer;
import edu.ukma.fin.ir.atsaruk.search.indices.Searchable;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class BiWordSearcher implements Searcher<String, BiWord, String> {

  private final Tokenizer<String> tokenizer;
  private final List<PostProcessor<String>> postProcessors;

  public BiWordSearcher(
      final Tokenizer<String> tokenizer,
      final List<PostProcessor<String>> postProcessors) {
    this.tokenizer = tokenizer;
    this.postProcessors = postProcessors;
  }

  @Override
  public List<String> search(Searchable<BiWord, String> source, String searchString) {
    final List<String> processedSearchString = tokenizer.tokenize(searchString).stream()
        .map(asPipe(postProcessors))
        .collect(Collectors.toList());
    if (processedSearchString.size() < 2) {
      throw new SearchException("Search string contains less than two words");
    }
    final BiWord query = ImmutableBiWord.builder()
        .first(processedSearchString.get(0))
        .second(processedSearchString.get(1))
        .build();

    return source.search(query);
  }
}
