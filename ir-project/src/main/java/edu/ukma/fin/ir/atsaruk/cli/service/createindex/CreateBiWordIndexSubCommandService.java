package edu.ukma.fin.ir.atsaruk.cli.service.createindex;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.BIWORD_SUB_COMMAND;
import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import edu.ukma.fin.ir.atsaruk.cli.service.SubCommandService;
import edu.ukma.fin.ir.atsaruk.dictionary.Dictionary;
import edu.ukma.fin.ir.atsaruk.dictionary.DictionaryCatalog;
import edu.ukma.fin.ir.atsaruk.dictionary.BiWord;
import edu.ukma.fin.ir.atsaruk.search.indices.Index;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CreateBiWordIndexSubCommandService implements SubCommandService {

  private static final Logger LOGGER = LoggerFactory.getLogger(
      CreateBiWordIndexSubCommandService.class);
  private static final String subCommandDescription = "Creates biword index based on biword dictionary catalog";

  private final DictionaryCatalog<String, BiWord> catalog;
  private final Index<BiWord, String> biWordIndex;

  public CreateBiWordIndexSubCommandService(
      final DictionaryCatalog<String, BiWord> dictionaryCatalog,
      final Index<BiWord, String> biWordIndex) {
    this.catalog = dictionaryCatalog;
    this.biWordIndex = biWordIndex;
  }

  @Override
  public Boolean apply(SubCommand subCommand) {
    LOGGER.debug("In create-index biword sub command service");

    catalog.getNames().forEach(name -> {
      final Dictionary<BiWord> dictionary = catalog.get(name);
      dictionary.getTokens().forEach(token -> biWordIndex.add(token, name));
    });

    return true;
  }

  @Override
  public String subCommandName() {
    return BIWORD_SUB_COMMAND;
  }

  @Override
  public String subCommandDescription() {
    return subCommandDescription;
  }

  @Override
  public String subCommandInfo() {
    return format("\t%s\t\t%s%n", subCommandName(), subCommandDescription());
  }
}
