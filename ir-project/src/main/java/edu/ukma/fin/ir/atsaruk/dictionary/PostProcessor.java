package edu.ukma.fin.ir.atsaruk.dictionary;

import java.util.function.Function;

public interface PostProcessor<T> extends Function<T, T> {

  default PostProcessor<T> andThen(PostProcessor<T> after) {
    return (T t) -> after.apply(apply(t));
  }
}
