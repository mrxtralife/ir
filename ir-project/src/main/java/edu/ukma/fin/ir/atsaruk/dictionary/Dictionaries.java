package edu.ukma.fin.ir.atsaruk.dictionary;

public final class Dictionaries {

  private Dictionaries() {
    //Util class
  }

  public static Dictionary<String> getStringDictionary() {
    return new StringDictionary();
  }

  public static Dictionary<BiWord> getBiWordDictionary() {
    return new BiWordDictionary();
  }

  public static Dictionary<PositionalWord> getPositionalWordDictionary() {
    return new PositionalWordDictionary();
  }
}
