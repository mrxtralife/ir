package edu.ukma.fin.ir.atsaruk.search.indices;

import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableCollection;
import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.stereotype.Component;

@Component
public class IncidenceMatrix implements Index<String, String> {

  private List<String> documents = new ArrayList<>();
  private Map<String, List<Integer>> matrix = new HashMap<>();

  @Override
  public void add(String key, String... values) {
    Arrays.stream(values).forEach(value -> add(key, value));
  }

  private void add(final String key, final String value) {
    int valueIndex = getValueIndex(value);
    if (valueIndex == -1) {
      valueIndex = addDocument(value);
    }
    List<Integer> row = getRow(key);
    row.remove(valueIndex);
    row.add(valueIndex, 1);
  }

  @Override
  public void add(String key, Collection<String> values) {
    values.forEach(value -> add(key, value));
  }

  @Override
  public void add(Map<String, Collection<String>> entries) {
    for (Entry<String, Collection<String>> entry : entries.entrySet()) {
      add(entry.getKey(), entry.getValue());
    }
  }

  @Override
  public void remove(final String key, String... values) {
    Arrays.stream(values).forEach(value -> remove(key, value));
  }

  private void remove(final String key, final String value) {
    int valueIndex = getValueIndex(value);
    if (valueIndex == -1) {
      return;
    }
    List<Integer> row = matrix.get(key);
    if (row == null) {
      return;
    }
    row.remove(valueIndex);
    row.add(valueIndex, 1);
  }

  @Override
  public void remove(String key, Collection<String> values) {
    values.forEach(value -> remove(key, value));
  }

  @Override
  public void remove(Map<String, Collection<String>> entries) {
    for (Entry<String, Collection<String>> entry : entries.entrySet()) {
      remove(entry.getKey(), entry.getValue());
    }
  }

  @Override
  public Collection<String> getKeys() {
    return unmodifiableCollection(matrix.keySet());
  }

  @Override
  public List<String> search(final String keyword) {
    final List<Integer> row = matrix.get(keyword);
    if (row == null) {
      return emptyList();
    }
    final List<String> values = new ArrayList<>();
    for (int i = 0; i < row.size(); i++) {
      if (row.get(i).equals(1)) {
        values.add(documents.get(i));
      }
    }
    return unmodifiableList(values);
  }

  @Override
  public List<String> searchNot(final String keyword) {
    final List<Integer> row = matrix.get(keyword);
    if (row == null) {
      return unmodifiableList(new ArrayList<>(documents));
    }

    final List<String> values = new ArrayList<>();
    for (int i = 0; i < row.size(); i++) {
      if (row.get(i).equals(0)) {
        values.add(documents.get(i));
      }
    }
    return unmodifiableList(values);
  }

  private int getValueIndex(final String value) {
    for (int i = 0; i < documents.size(); i++) {
      if (documents.get(i).equals(value)) {
        return i;
      }
    }
    return -1;
  }

  private int addDocument(final String value) {
    documents.add(value);
    matrix.values().forEach(row -> row.add(0));
    return documents.size() - 1;
  }

  private List<Integer> getRow(final String key) {
    List<Integer> row = matrix.get(key);

    if (row == null) {
      row = Stream.iterate(0, n -> n).limit(documents.size()).collect(Collectors.toList());
      matrix.put(key, row);
    }
    return row;
  }
}
