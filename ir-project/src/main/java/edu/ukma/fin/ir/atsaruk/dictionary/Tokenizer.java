package edu.ukma.fin.ir.atsaruk.dictionary;

import java.util.List;
import java.util.function.Function;

public interface Tokenizer<T> extends Function<T, List<T>> {

  List<T> tokenize(final T text);

  @Override
  default List<T> apply(T t) {
    return tokenize(t);
  }
}
