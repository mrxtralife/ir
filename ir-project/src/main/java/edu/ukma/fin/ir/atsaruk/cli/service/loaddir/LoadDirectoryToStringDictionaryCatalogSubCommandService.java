package edu.ukma.fin.ir.atsaruk.cli.service.loaddir;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.STRING_SUB_COMMAND;
import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import edu.ukma.fin.ir.atsaruk.cli.service.SubCommandService;
import edu.ukma.fin.ir.atsaruk.dictionary.service.StringDictionaryCatalogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class LoadDirectoryToStringDictionaryCatalogSubCommandService implements SubCommandService {

  private static final Logger LOGGER = LoggerFactory.getLogger(
      LoadDirectoryToStringDictionaryCatalogSubCommandService.class);
  private static final String subCommandDescription = "Loads directory content and creates string dictionary catalog";

  private final StringDictionaryCatalogService catalogService;

  public LoadDirectoryToStringDictionaryCatalogSubCommandService(
      final StringDictionaryCatalogService catalogService) {
    this.catalogService = catalogService;
  }

  @Override
  public Boolean apply(SubCommand subCommand) {
    LOGGER.debug("In load-dir string sub command service");

    for (final String directory : subCommand.arguments().split("\\s+")) {
      catalogService.loadDirectoryContent(directory);
    }

    return true;
  }

  @Override
  public String subCommandName() {
    return STRING_SUB_COMMAND;
  }

  @Override
  public String subCommandDescription() {
    return subCommandDescription;
  }

  @Override
  public String subCommandInfo() {
    return format("\t%s\t\t%s%n", subCommandName(), subCommandDescription());
  }
}
