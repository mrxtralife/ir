package edu.ukma.fin.ir.atsaruk.search;

import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.List;

public class BooleanSearchQuery {

  private final List<BooleanSearchQueryComponent> components;

  public BooleanSearchQuery() {
    this.components = new ArrayList<>();
  }

  public BooleanSearchQuery(final BooleanSearchQueryComponent component) {
    this();
    components.add(component);
  }

  public void addComponent(final BooleanSearchQueryComponent component) {
    this.components.add(component);
  }

  public List<BooleanSearchQueryComponent> getComponents() {
    return unmodifiableList(new ArrayList<>(components));
  }
}
