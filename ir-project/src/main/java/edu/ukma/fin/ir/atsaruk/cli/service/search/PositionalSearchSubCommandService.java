package edu.ukma.fin.ir.atsaruk.cli.service.search;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.POSITIONAL_SUB_COMMAND;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.printToConsole;
import static edu.ukma.fin.ir.atsaruk.dictionary.PostProcessorUtils.asPipe;
import static java.lang.String.format;

import edu.ukma.fin.ir.atsaruk.cli.CliException;
import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import edu.ukma.fin.ir.atsaruk.cli.service.SubCommandService;
import edu.ukma.fin.ir.atsaruk.dictionary.PostProcessor;
import edu.ukma.fin.ir.atsaruk.dictionary.Tokenizer;
import edu.ukma.fin.ir.atsaruk.search.indices.PositionalIndex;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class PositionalSearchSubCommandService implements SubCommandService {

  private static final Logger LOGGER = LoggerFactory.getLogger(
      PositionalSearchSubCommandService.class);
  private static final String subCommandDescription =
      "Performs positional search within positional index. "
          + "Syntax: " + POSITIONAL_SUB_COMMAND
          + " -dX term1 term2 term3 ... (X - distance value)";

  private final PositionalIndex index;
  private final Tokenizer<String> tokenizer;
  private final List<PostProcessor<String>> postProcessors;

  public PositionalSearchSubCommandService(final PositionalIndex positionalIndex,
      final Tokenizer<String> tokenizer, final List<PostProcessor<String>> postProcessors) {
    this.index = positionalIndex;
    this.tokenizer = tokenizer;
    this.postProcessors = postProcessors;
  }

  @Override
  public Boolean apply(SubCommand subCommand) {
    LOGGER.debug("In search positional sub command service");

    final String arguments = subCommand.arguments();
    if (arguments.isEmpty()) {
      throw new CliException("Empty search arguments");
    }

    final String[] decArgs = arguments.split("\\s+", 2);

    if (decArgs.length != 2) {
      throw new CliException("Incorrect arguments");
    }
    final long distance = tryParseDistance(decArgs[0]);

    final List<String> searchQuery = tokenizer.tokenize(decArgs[1]).stream()
        .map(asPipe(postProcessors))
        .collect(Collectors.toList());

    final List<String> searchResults = index.search(searchQuery, distance);

    printToConsole(searchResults);

    return true;
  }

  private long tryParseDistance(final String arg) {
    if (!arg.startsWith("-d")) {
      throw new CliException("Distance option is not found.");
    }
    try {
      return Long.valueOf(arg.substring(2));
    } catch (NumberFormatException ex) {
      throw new CliException("Wrong distance value", ex);
    }
  }

  @Override
  public String subCommandName() {
    return POSITIONAL_SUB_COMMAND;
  }

  @Override
  public String subCommandDescription() {
    return subCommandDescription;
  }

  @Override
  public String subCommandInfo() {
    return format("\t%s\t\t%s%n", subCommandName(), subCommandDescription());
  }
}
