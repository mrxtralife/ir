package edu.ukma.fin.ir.atsaruk.cli.service;

import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.BIWORD_SUB_COMMAND;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.INCIDENCE_MATRIX_SUB_COMMAND;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.INVERTED_INDEX_SUB_COMMAND;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.POSITIONAL_SUB_COMMAND;
import static edu.ukma.fin.ir.atsaruk.cli.CliUtils.STRING_SUB_COMMAND;

import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import java.util.function.Predicate;

public enum SubCommandRule implements Predicate<SubCommand> {
  STRING_SUB_COMMAND_RULE(subCommand -> STRING_SUB_COMMAND.equals(subCommand.subCommand())),

  BIWORD_SUB_COMMAND_RULE(subCommand -> BIWORD_SUB_COMMAND.equals(subCommand.subCommand())),

  POSITIONAL_SUB_COMMAND_RULE(subCommand -> POSITIONAL_SUB_COMMAND.equals(subCommand.subCommand())),

  INVERTED_INDEX_SUB_COMMAND_RULE(
      subCommand -> INVERTED_INDEX_SUB_COMMAND.equals(subCommand.subCommand())),

  INCIDENCE_MATRIX_SUB_COMMAND_RULE(
      subCommand -> INCIDENCE_MATRIX_SUB_COMMAND.equals(subCommand.subCommand()));

  private final Predicate<SubCommand> rule;

  SubCommandRule(Predicate<SubCommand> rule) {
    this.rule = rule;
  }

  @Override
  public boolean test(SubCommand command) {
    return rule.test(command);
  }
}
