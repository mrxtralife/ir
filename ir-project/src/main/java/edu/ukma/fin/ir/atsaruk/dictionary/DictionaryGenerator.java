package edu.ukma.fin.ir.atsaruk.dictionary;

public interface DictionaryGenerator<S, T> {

  Dictionary<T> generate(final S source);
}
