package edu.ukma.fin.ir.atsaruk.cli.config;

import static edu.ukma.fin.ir.atsaruk.cli.service.SubCommandRule.BIWORD_SUB_COMMAND_RULE;
import static edu.ukma.fin.ir.atsaruk.cli.service.SubCommandRule.INCIDENCE_MATRIX_SUB_COMMAND_RULE;
import static edu.ukma.fin.ir.atsaruk.cli.service.SubCommandRule.INVERTED_INDEX_SUB_COMMAND_RULE;
import static edu.ukma.fin.ir.atsaruk.cli.service.SubCommandRule.POSITIONAL_SUB_COMMAND_RULE;
import static edu.ukma.fin.ir.atsaruk.cli.service.SubCommandRule.STRING_SUB_COMMAND_RULE;

import edu.ukma.fin.ir.atsaruk.cli.entity.SubCommand;
import edu.ukma.fin.ir.atsaruk.cli.service.CommandService;
import edu.ukma.fin.ir.atsaruk.cli.service.SubCommandService;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SubCommandHandlersConfiguration {

  public SubCommandHandlersConfiguration(
      final Map<Predicate<SubCommand>, SubCommandService> loadDirSubCommandHandlers,
      final SubCommandService loadDirectoryToStringDictionaryCatalogSubCommandService,
      final SubCommandService loadDirectoryToBiWordDictionaryCatalogSubCommandService,
      final SubCommandService loadDirectoryToPositionalWordDictionaryCatalogSubCommandService,

      final Map<Predicate<SubCommand>, SubCommandService> createIndexSubCommandHandlers,
      final SubCommandService createInvertedIndexSubCommandService,
      final SubCommandService createIncidenceMatrixSubCommandService,
      final SubCommandService createBiWordIndexSubCommandService,
      final SubCommandService createPositionalIndexSubCommandService,

      final Map<Predicate<SubCommand>, SubCommandService> simpleSearchSubCommandHandlers,
      final SubCommandService simpleSearchInvertedIndexSubCommandService,
      final SubCommandService simpleSearchIncidenceMatrixSubCommandService,

      final Map<Predicate<SubCommand>, SubCommandService> searchSubCommandHandlers,
      final SubCommandService booleanSearchInvertedIndexSubCommandService,
      final SubCommandService booleanSearchIncidenceMatrixSubCommandService,
      final SubCommandService biWordSearchSubCommandService,
      final SubCommandService positionalSearchSubCommandService,

      final Map<String, String> commands,
      final List<CommandService> commandServices
  ) {
    loadDirSubCommandHandlers
        .put(STRING_SUB_COMMAND_RULE, loadDirectoryToStringDictionaryCatalogSubCommandService);
    loadDirSubCommandHandlers
        .put(BIWORD_SUB_COMMAND_RULE, loadDirectoryToBiWordDictionaryCatalogSubCommandService);
    loadDirSubCommandHandlers
        .put(POSITIONAL_SUB_COMMAND_RULE,
            loadDirectoryToPositionalWordDictionaryCatalogSubCommandService);

    createIndexSubCommandHandlers
        .put(INVERTED_INDEX_SUB_COMMAND_RULE, createInvertedIndexSubCommandService);
    createIndexSubCommandHandlers
        .put(INCIDENCE_MATRIX_SUB_COMMAND_RULE, createIncidenceMatrixSubCommandService);
    createIndexSubCommandHandlers
        .put(BIWORD_SUB_COMMAND_RULE, createBiWordIndexSubCommandService);
    createIndexSubCommandHandlers
        .put(POSITIONAL_SUB_COMMAND_RULE, createPositionalIndexSubCommandService);

    simpleSearchSubCommandHandlers
        .put(INVERTED_INDEX_SUB_COMMAND_RULE, simpleSearchInvertedIndexSubCommandService);
    simpleSearchSubCommandHandlers
        .put(INCIDENCE_MATRIX_SUB_COMMAND_RULE, simpleSearchIncidenceMatrixSubCommandService);

    searchSubCommandHandlers
        .put(INVERTED_INDEX_SUB_COMMAND_RULE, booleanSearchInvertedIndexSubCommandService);
    searchSubCommandHandlers
        .put(INCIDENCE_MATRIX_SUB_COMMAND_RULE, booleanSearchIncidenceMatrixSubCommandService);
    searchSubCommandHandlers
        .put(BIWORD_SUB_COMMAND_RULE, biWordSearchSubCommandService);
    searchSubCommandHandlers
        .put(POSITIONAL_SUB_COMMAND_RULE, positionalSearchSubCommandService);

    commands.putAll(commandServices.stream().collect(
        Collectors.toMap(CommandService::commandName, CommandService::commandInfo)));
  }
}
