package edu.ukma.fin.ir.atsaruk.search.indices;

import java.util.List;

public interface Searchable<Q, R> {

  List<R> search(final Q query);

  List<R> searchNot(final Q query);
}