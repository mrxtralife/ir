package edu.ukma.fin.ir.atsaruk.dictionary;

import java.util.Collection;

public interface Dictionary<T> {

  void add(final T token);

  void add(final Collection<T> tokens);

  void remove(final T token);

  void remove(final Collection<T> tokens);

  Collection<T> getTokens();
}
