package edu.ukma.fin.ir.atsaruk.search;

import static com.google.common.truth.Truth.assertThat;

import edu.ukma.fin.ir.atsaruk.common.Parser;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class BooleanSearchArgumentParserTest {

  private Parser<String, BooleanSearchQuery> parser;

  @BeforeMethod
  public void setUp() {
    parser = new BooleanSearchArgumentParser();
  }

  @Test
  public void testParse() {
    final String args = "NOT Putin AND Russia OR Ukraine AND NOT Usa";

    BooleanSearchQuery query = parser.parse(args);

    assertThat(query).isNotNull();
  }
}
