package edu.ukma.fin.ir.atsaruk.cli;

import static com.google.common.truth.Truth.assertThat;

import edu.ukma.fin.ir.atsaruk.cli.entity.Command;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CommandParserTest {

  private CommandParser parser;

  @BeforeMethod
  public void setUp() {
    parser = new CommandParser();
  }

  @Test
  public void testParse() {
    final String strCommand = "help";

    final Command actual = parser.parse(strCommand);

    assertThat(actual).isNotNull();
    assertThat(actual.command()).isEqualTo("help");
    assertThat(actual.subCommand()).isEqualTo("");
    assertThat(actual.arguments()).isNotNull();
    assertThat(actual.arguments()).isEmpty();
  }

  @Test
  public void testParseEmptyString() {
    final String strCommand = "";

    final Command actual = parser.parse(strCommand);

    assertThat(actual).isNotNull();
    assertThat(actual.command()).isEqualTo("");
    assertThat(actual.subCommand()).isEqualTo("");
    assertThat(actual.arguments()).isNotNull();
    assertThat(actual.arguments()).isEmpty();
  }

  @Test(expectedExceptions = AssertionError.class)
  public void testParseNull() {
    final Command actual = parser.parse(null);

    assertThat(actual).isNotNull();
  }

  @Test
  public void testParseLongCommand() {
    final String strCommand = "search simple Russia AND NOT Ukraine";

    final Command actual = parser.parse(strCommand);

    assertThat(actual).isNotNull();
  }
}
