package edu.ukma.fin.ir.atsaruk.search.indices;

import static com.google.common.truth.Truth.assertThat;

import edu.ukma.fin.ir.atsaruk.dictionary.ImmutablePositionalWord;
import edu.ukma.fin.ir.atsaruk.dictionary.LowercasePostProcessor;
import edu.ukma.fin.ir.atsaruk.dictionary.PostProcessor;
import edu.ukma.fin.ir.atsaruk.dictionary.SimpleEnglishWordTokenizer;
import edu.ukma.fin.ir.atsaruk.dictionary.Tokenizer;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.testng.annotations.Test;

public class PositionalIndexTest {

  @Test
  public void testSearch() {
    final String testString =
        "When I saw him in this state, I saw that for the poor man’s sake, as well\n"
            + "as for the public safety, what I had to do for the time was to compose\n"
            + "his mind.  Therefore, setting aside all question of reality or unreality\n"
            + "between us, I represented to him that whoever thoroughly discharged his\n"
            + "duty must do well, and that at least it was his comfort that he\n"
            + "understood his duty, though he did not understand these confounding\n"
            + "Appearances.  In this effort I succeeded far better than in the attempt\n"
            + "to reason him out of his conviction.  He became calm; the occupations\n"
            + "incidental to his post as the night advanced began to make larger demands\n"
            + "on his attention: and I left him at two in the morning.  I had offered to\n"
            + "stay through the night, but he would not hear of it.\n";

    final PositionalIndex index = new PositionalIndex();
    final Tokenizer<String> tokenizer = new SimpleEnglishWordTokenizer();
    final PostProcessor<String> postProcessor = new LowercasePostProcessor();
    final AtomicLong position = new AtomicLong(0);
    tokenizer.tokenize(testString).stream().map(postProcessor)
        .filter(str -> !str.isEmpty())
        .map(word -> ImmutablePositionalWord.builder()
            .word(word)
            .position(position.getAndIncrement())
            .build())
        .forEach(word -> index.add(word, "testDictionary"));

    List<String> result = index.search(Arrays.asList("saw", "the", "this"), 3);
    assertThat(result).isNotNull();
    assertThat(result).isNotEmpty();
    assertThat(result).hasSize(1);
  }
}
