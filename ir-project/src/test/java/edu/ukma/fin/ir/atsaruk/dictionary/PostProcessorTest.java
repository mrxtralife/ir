package edu.ukma.fin.ir.atsaruk.dictionary;

import static com.google.common.truth.Truth.assertThat;

import java.util.ArrayList;
import java.util.List;
import org.testng.annotations.Test;

public class PostProcessorTest {


  @Test
  public void testPostProcessors() {
    PostProcessor<String> lowercase = str -> {
      if (str == null) {
        return null;
      }
      return str.toLowerCase();
    };

    PostProcessor<String> add123 = str -> {
      if (str == null) {
        return null;
      }
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < str.length(); i++) {
        sb.append(str.charAt(i)).append("123");
      }
      return sb.toString();
    };

    PostProcessor<String> uppercase = str -> {
      if (str == null) {
        return null;
      }
      return str.toUpperCase();
    };

    PostProcessor<String> everyTwo = str -> {
      if (str == null) {
        return null;
      }
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < str.length(); i++) {
        if (i % 2 == 0) {
          sb.append(str.charAt(i));
        }
      }
      return sb.toString();
    };

    List<PostProcessor<String>> postProcessors = new ArrayList<>();

    postProcessors.add(lowercase);
    postProcessors.add(add123);
    postProcessors.add(uppercase);
    postProcessors.add(everyTwo);

    String testStr = "Character equivalent. This can lead to, as there WILL be no Need to box each integer into a Character object";

    PostProcessor<String> pipe = postProcessors.stream().reduce(PostProcessor::andThen)
        .orElse(s -> s);

    PostProcessor<String> reversePipe = everyTwo.andThen(uppercase).andThen(add123)
        .andThen(lowercase);

    final String pipeResult = pipe.apply(testStr);

    assertThat(pipeResult).isNotNull();
    assertThat(pipeResult).isNotEmpty();
    assertThat(pipeResult).isEqualTo(
        "C2H2A2R2A2C2T2E2R2 2E2Q2U2I2V2A2L2E2N2T2.2 2T2H2I2S2 2C2A2N2 2L2E2A2D2 2T2O2,2 2A2S2 2T2H2E2R2E2 2W2I2L2L2 2B2E2 2N2O2 2N2E2E2D2 2T2O2 2B2O2X2 2E2A2C2H2 2I2N2T2E2G2E2R2 2I2N2T2O2 2A2 2C2H2A2R2A2C2T2E2R2 2O2B2J2E2C2T2");

    final String reversePipeResult = reversePipe.apply(testStr);

    assertThat(reversePipeResult).isNotNull();
    assertThat(reversePipeResult).isNotEmpty();
    assertThat(reversePipeResult).isEqualTo(
        "c123a123a123t123r123e123u123v123l123n123.123t123i123 123a123 123e123d123t123,123a123 123h123r123 123i123l123b123 123o123n123e123 123o123b123x123e123c123 123n123e123e123 123n123o123a123c123a123a123t123r123o123j123c123");
  }

}
